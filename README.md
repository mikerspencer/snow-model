## Degree day snow model

Using daily average temperature and daily precipitation to model snow accumulation and melt over a winter season.

### Files

* **Hough_melt.R** Outputs melt grid as specifed in Hough 1997 paper
* **model.R** Snowmelt model, takes single dimension vector as input
* **Station_model.Rmd** Runs model diagnostics, probably doesn't work as a whole file
* **run.R** what I should be using for my model runs...
* **NetCDF.R** reads netcdf precip and raster temp, gets both alligned to same grid and writes out to a new netcdf file ready for a gridded model run
* **plots.R** generates model output plots
* **functions.R** munging functions for data processing

