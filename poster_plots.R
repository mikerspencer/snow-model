# ---------------------------------------
# Plots for ICAM poster
# ---------------------------------------

# ---------------------------------------
# Fig 3 MOst parameter space
# ---------------------------------------
# Depends on Station_model_multi.Rmd

pdf("./poster/figures/parameters_penalty.pdf", height=13, width=9)
# DDF
par(mfcol=c(2,1), mar=c(4, 7, 4, 2) + 0.1, cex=1.5) #c(bottom, left, top, right)
boxplot(DDFi ~ Station, best.params, xlab="DDF", main="MOst parameter space", horizontal=T, las=1, lwd=3)
# Temp.b
par(mar=c(5, 7, 3, 2) + 0.1) #c(bottom, left, top, right)
boxplot(temp.bi ~ Station, best.params, xlab="Temp.b", horizontal=T, las=1, lwd=3)
dev.off()


# ---------------------------------------
# Fig 4 Calibration/verification performance
# ---------------------------------------
# Depends on grid_calibration.R

pdf("./poster/figures/calibration_verification.pdf", height=13, width=9)
# Calibration
par(mfcol=c(2,1), mar=c(4, 5, 4, 2) + 0.1, cex=1.5) #c(bottom, left, top, right)
boxplot(perf~elev, full[full$year < 1991,], ylab="Elevation band (m)", main="Calibration performance", horizontal=T, las=1, lwd=3)
# Verification
boxplot(perf~elev, full[full$year > 1990,], ylab="Elevation band (m)", main="Verification performance", horizontal=T, las=1, lwd=3)
dev.off()