# ---------------------------------------
# Calculate melt grids from Hough & Hollis 1997
# ---------------------------------------
# Taken from table 7 (p136) in
# 'Rare snowmelt estimation in the United Kingdom'

# ---------------------------------------
# Set up
# ---------------------------------------
setwd("~/Cloud/Michael/Uni_temp/Snow_mod")
library(raster)
library(RColorBrewer)
library(rasterVis)
library(lattice)
library(grid)

# ---------------------------------------
# read rasters
# ---------------------------------------
DEM = raster("/home/workbackup/work/GIS/OS/Terrain50")
# DEM = raster("~/GIS/OS/Terrain50")
# max Jan mean daily ave temp
MAX = raster("~/Cloud/Michael/Uni_temp/Snow_mod/data/MaxTemp_Jan_1981-2010_LTA.txt")
# mean Jan windspeed (knots at 10 m above ground)
Wind = raster("~/Cloud/Michael/Uni_temp/Snow_mod/data/MeanWindSpeed_Jan_1981-2010_LTA.txt")

# Resample DEM to 5km
DEM = resample(DEM, MAX, "bilinear")
DEM = mask(DEM, MAX)
DEM = mask(DEM, DEM, maskvalue = 0)

# Remove NI as no elevation data
MAX = mask(MAX, DEM)
Wind = mask(Wind, DEM)

ex = extent(0, 7e+05, 0, 1250000)
DEM = crop(DEM, ex)
MAX = crop(MAX, ex)
Wind = crop(Wind, ex)


# ---------------------------------------
# Single non-weather factor
# ---------------------------------------
melt.1.nw = 5.09 + .085 * DEM

# ---------------------------------------
# Double non-weather factor
# ---------------------------------------
North = coordinates(DEM)
North = cbind(North, z = round(North[,2]/100))
North = rasterFromXYZ(North)
North = mask(North, DEM)

melt.2.nw = -3.8 + .083 * DEM + .00187 * North

# ---------------------------------------
# Single weather factor
# ---------------------------------------
melt.1.w = 71.16 - 9.45 * MAX

# ---------------------------------------
# Double weather factor
# ---------------------------------------
melt.2.w = 52.52 - 9.08 * MAX + 1.46 * Wind

# ---------------------------------------
# Plot results
# ---------------------------------------
# base graphics (no standarised legend)
# pal = colorRampPalette(brewer.pal(4, "Blues"))
# png("results/Hough_melt.png", 600, 750)
# par(mfrow=c(2,2), mar=c(5, 4, 4, 8) + 0.1, cex=1.2)
# plot(melt.1.nw, col=pal(255), main="Single non-weather")
# par(cex=1.2)
# plot(melt.2.nw, col=pal(255), main="Double non-weather")
# par(cex=1.2)
# plot(melt.1.w, col=pal(255), main="Single weather", cex=1.2)
# par(cex=1.2)
# plot(melt.2.w, col=pal(255), main="Double weather")
# dev.off()
# 
# # rasterVis (standarised legend)
# png("results/Hough_melt.png", 550, 750, res=100)
# x = stack(melt.1.nw, melt.2.nw, melt.1.w, melt.2.w)
# myTheme=rasterTheme(region=brewer.pal('Blues', n=9))
# 
# levelplot(x, names.attr=c("Single non weather", "Double non weather", "Single weather", "Double weather"), par.settings=myTheme, layout=c(2,2))
# trellis.focus("legend", side="right", clipp.off=T, highlight=F)
# grid.text("Melt\n(mm/day)", 0.25, 0, hjust=0.5, vjust=1.2)
# trellis.unfocus()
# dev.off()
