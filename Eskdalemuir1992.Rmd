---
title: "Eskdalemuir 1992"
author: "Mike Spencer"
date: "21 October 2015"
output: html_document
---


```{r data}
library(RSQLite)

db = dbConnect(SQLite(), dbname="~/Cloud/Michael/Uni_temp/MIDAS/data/MIDAS_select.sqlite")

snow = dbGetQuery(db, "SELECT DATE(Date) AS Date, LyingSnow, SnowDepth, FreshSnowDepth, SnowCode FROM snow WHERE station=1023 AND HydroYear=1992")
precip = dbGetQuery(db, "SELECT DATE(Date) AS Date, Precip FROM precip WHERE station=1023 AND HydroYear=1992")
temp = dbGetQuery(db, "SELECT DATE(Date) AS Date, MIN(MinAirTemp) AS MinTemp, MAX(MaxAirTemp) AS MaxTemp FROM temperature WHERE station=1023 AND HydroYear=1992 GROUP BY DATE(Date)")
dbDisconnect(db)
rm(db)

df = merge(snow, precip)
df = merge(df, temp)
df$Date = as.Date(df$Date)
```



```{r plots, fig.height=11, fig.width=8}
par(mfrow=c(3, 1))
plot(SnowDepth ~ Date, df, type="l", main="full year", xlab="")
plot(Precip ~ Date, df, type="l", xlab="")
plot(MinTemp ~ Date, df, type="l", xlab="")

plot(SnowDepth ~ Date, df[1:230, ], type="l", main="winter", xlab="")
plot(Precip ~ Date, df[1:230, ], type="l", xlab="")
plot(MinTemp ~ Date, df[1:230, ], type="l", xlab="")

plot(SnowDepth ~ Date, df[75:120, ], type="l", main="snowiest period", xlab="")
plot(Precip ~ Date, df[75:120, ], type="l", xlab="")
plot(MinTemp ~ Date, df[75:120, ], type="l", xlab="")

par(mfrow=c(1, 1))
```

