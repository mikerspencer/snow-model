# Complete winters
source("~/Cloud/Michael/Uni_temp/Snow_mod/repo/inputs.R")


st.full = dbGetQuery(MIDAS.db, "SELECT DISTINCT(Station) FROM snow")

complete = lapply(st.full[,1], function(i){
   print(i)
   x = counter(i)
   if(nrow(x)>0){
      data.frame(ID=i,
                 HydroYear=x$HydroYear,
                 stringsAsFactors=F)
   }
})
# Bind to df
complete = do.call("rbind", complete)
