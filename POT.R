# ------------------------------------------
# ------------------------------------------
# Get peaks over threshold melt events per annum
# ------------------------------------------
# ------------------------------------------

# ------------------------------------------
# Setup
# ------------------------------------------
setwd("~/Cloud/Michael/Uni_temp/Snow_mod")
#install.packages("Rcpp")
#install.packages("dplyr")
library(ncdf4)
library(raster)
library(rgdal)
library(parallel)
library(dplyr)
source("./repo/functions.R")


# ------------------------------------------
# Get POT count for each cell and write to tif
# ------------------------------------------

# Get index of data cells
nc.grid = read.csv("./data/nc_grid_index.csv")

sm.pot = function(year, thresh){
   x = lapply(1:nrow(nc.grid), function(i){
      nc = nc_open(paste0("~/Cloud/Michael/Uni_temp/Snow_mod/results/grid/full_", year, ".nc"))
      sm = ncvar_get(nc, "Snowmelt", start=c(nc.grid[i, 1], nc.grid[i, 2], 1), count=c(1, 1, -1))
      nc_close(nc)
      data.frame(nc.grid[i, ], POT=sum(sm > thresh))
   })
   x = do.call("rbind.data.frame", x)
   x = rasterFromXYZ(x[,3:5])
   writeRaster(x, paste0("./results/raster/pot/POT_", year, ".tif"), overwrite=T)
}

# ------------------------------------------
# Run for all years
# ------------------------------------------

mclapply(1960:2010, mc.cores=4, function(i){sm.pot(i, 42)})


# ------------------------------------------
# Check results
# ------------------------------------------

f = list.files("./results/raster/pot", full.names=T, pattern="POT")

rl = lapply(f, raster)
y = round(sapply(rl, function(i){max(getValues(i), na.rm=T)}), 0)
plot(1960:2010, y, xlab="Year", ylab="Days melt exceeded 42 mm/day", type="l")

